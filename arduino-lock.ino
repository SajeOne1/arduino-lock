//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// Author: Shane Brown
// Date: 15/06/2016
// Description: Input and lock control code
// Revision: 2 - Add EEPROM support
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

#include <SPI.h>
#include <MFRC522.h>
#include <EEPROM.h>

#define RST_PIN			9
#define SS_PIN			10

MFRC522 mfrc522(SS_PIN, RST_PIN);

MFRC522::MIFARE_Key key;

// PINS
int buttonPin = A5; // Analog Input for Keypad
int lockPin = 2; // Lock Control Output(HIGH == UNLOCKED)

// KEYS
int* privateKey; int privKeySize; // Private Key From EEPROM
int debugKey[4] = {4, 3, 2, 1};
int bufferKey[4] = { -1, -1, -1, -1}; // Storage Space for Entered Password

// STATES
int lockState = 1; // BOOL
int reset = 0; // BOOL
int optionsMenuCount = 0; // INT

void setup() {
  Serial.begin(9600); // Initialize serial communications with the PC
  while (!Serial);	  // Do nothing if no serial port is opened (added for Arduinos based on ATMEGA32U4)
  SPI.begin();		  // Init SPI bus
  mfrc522.PCD_Init(); // Init MFRC522 card

  for (byte i = 0; i < 6; i++) {
    key.keyByte[i] = 0xFF;
  }
  dump_byte_array(key.keyByte, MFRC522::MF_KEY_SIZE);

  pinMode(lockPin, OUTPUT);

  //saveKeyToEEPROM(debugKey, sizeof(debugKey) / sizeof(int));

  // Get Key from EEPROM
  privateKey = getKeyFromEEPROM(privKeySize);
  Serial.print("Size of key:");
  Serial.println(privKeySize);
  for (int i = 0; i < privKeySize; i++) {
    Serial.print(privateKey[i]);
  }
}

void loop() {
  handleKeypad();
  handleRFID();
}

/* HELPER FUNCTIONS -- START */
void handleRFID() {
  if (reset) {
    Serial.println("--- WRITE KEY");
    if(writeToRFID(privateKey, privKeySize))
        reset = 0;
  }
  else {
    int keySize;
    byte* myKey = readFromRFID(keySize);
    if (myKey) {
      Serial.println("--- READ KEY");
      int* myInts = byteArrToIntArr(myKey, keySize);
      if (evalKey(myInts, keySize)) {
        Serial.println("Good Key");
        cycleLock();
      } else {
        Serial.println("Bad Key");
      }
    }
  }
}

void handleKeypad() {
  // Get initial analog value from keypad
  int value = analogRead(buttonPin);
  //Serial.println(value);

  // Map raw value to assigned key
  int key = getKey(value);

  // If valid key perform operations
  if (key > -1) {
    Serial.print("Button: ");
    Serial.println(key);
    switch (key) {
      case 10: // Clear(Star)
        clearKey(bufferKey, sizeof(bufferKey) / sizeof(int));
        Serial.println("Star: Cleared");
        break;
      case 11: // Enter(Pound)
        Serial.println("Pound: Code Entered");
        handlePoundPressed();
        break;
      default:
        addToKey(key, bufferKey, sizeof(bufferKey) / sizeof(int));
    }
    delay(300);
    Serial.print("Key Size: ");
    Serial.println(keyLen(bufferKey, sizeof(bufferKey) / sizeof(int)));
  }
}

void handlePoundPressed() {
  if (lockState) { // LOCKED
    if (evalKey(bufferKey, sizeof(bufferKey) / sizeof(int))) {
      Serial.println("Good Key");
      cycleLock();
    } else {
      Serial.println("Bad Key");
    }
  } else { // UNLOCKED
    if (reset) { // RESET MODE
      if (keyLen(bufferKey, sizeof(bufferKey) / sizeof(int)) == 4) {
        Serial.print("Saved Key: ");
        for (int i = 0; i < sizeof(bufferKey) / sizeof(int); i++)
          Serial.print(bufferKey[i]);
        Serial.println();
        saveKeyToEEPROM(bufferKey, sizeof(bufferKey) / sizeof(int));
        memcpy(privateKey, bufferKey, sizeof(bufferKey)); // Need to re-assign privateKey for new password
        reset = 0;
      } else {
        Serial.println("Bad key, not saving");
        reset = 0;
      }
    } else if (keyLen(bufferKey, sizeof(bufferKey) / sizeof(int)) == 0) { // NOT RESET MODE, ATTEMPT TO ENTER
      Serial.print("Attempt to enter: ");
      Serial.println(optionsMenuCount);
      optionsMenuCount++;
      if (optionsMenuCount >= 3) {
        Serial.println("Reset Mode Started");
        optionsMenuCount = 0;
        reset = 1;
      }
    }
    else { // NOT RESET, ATTEMPT TO LOCK
      if (evalKey(bufferKey, sizeof(bufferKey) / sizeof(int))) {
        Serial.println("Good Key");
        cycleLock();
      } else {
        Serial.println("Bad Key");
      }
    }
  }

  // Clear the password after entry
  clearKey(bufferKey, sizeof(bufferKey) / sizeof(int));
}

int* byteArrToIntArr(byte arr1[], int keySize) {
  int* byteArr = (int*)malloc(keySize);
  for (int i = 0; i < keySize; i++) {
    if (arr1[i] > 127 || arr1[i] < -128) // OUTSIDE RANGE OF BYTE
      return 0;

    byteArr[i] = (int)arr1[i];
  }

  return byteArr;
}

byte* intArrToByteArr(int arr1[], int keySize) {
  byte* byteArr = (byte*)malloc(keySize);
  for (int i = 0; i < keySize; i++) {
    if (arr1[i] > 127 || arr1[i] < -128) // OUTSIDE RANGE OF BYTE
      return 0;

    byteArr[i] = (byte)arr1[i];
  }

  return byteArr;
}
/* HELPER FUNCTIONS -- END */

/* KEY UTILS -- START */
int addToKey(int digit, int key[], int keySize) {
  int curKeyLen = keyLen(key, keySize);
  if (curKeyLen == 4) // Key Full
    return 0;

  key[curKeyLen] = digit;

}

// Evaluate correctness of given key
int evalKey(int key[], int keySize) {
  if (privKeySize != keySize) {
    Serial.println("Bad Size");
    return 0;
  }

  for (int i = 0; i < keySize; i++)
    if (privateKey[i] != key[i]) {
      Serial.println("Bad Buttons");
      return 0;
    }

  return 1;
}

// Get length of actual key values
int keyLen(int key[], int keySize) {
  int count = 0;
  for (int i = 0; i < keySize; i++) {
    if (key[i] != -1)
      count++;
  }

  return count;
}

void clearKey(int key[], int keySize) {
  for (int i = 0; i < keySize; i++)
    key[i] = -1;
}

int getKey(int value) {
  if (value >= 670 && value <= 685) return 1;
  else if (value >= 613 && value <= 623) return 2;
  else if (value >= 550 && value <= 590) return 3;
  else if (value >= 510 && value <= 535) return 4;
  else if (value >= 481 && value <= 488) return 5;
  else if (value >= 450 && value <= 455) return 6;
  else if (value >= 420 && value <= 427) return 7;
  else if (value >= 396 && value <= 402) return 8;
  else if (value >= 375 && value <= 380) return 9;
  else if (value >= 354 && value <= 360) return 10; // Star
  else if (value >= 336 && value <= 342) return 0;
  else if (value >= 320 && value <= 330) return 11; // Pound
  else return -1;
}
/* KEY UTILS -- END */

/* LOCK UTILS -- START */
void cycleLock() {
  digitalWrite(lockPin, lockState);
  lockState = !lockState;
}

void unlock() {
  digitalWrite(lockPin, HIGH);
  lockState = 0;
}

void lock() {
  digitalWrite(lockPin, HIGH);
  lockState = 1;
}
/* LOCK UTILS -- END */

/* EEPROM UTILS -- START */
void saveKeyToEEPROM(int key[], int keySize) {
  Serial.print("EEPROM Size: ");
  Serial.println(keySize);
  EEPROM.write(0, keySize); // Store length of key
  for (int i = 0; i < keySize; i++) {
    EEPROM.write(i + 1, key[i]);
  }
}

int* getKeyFromEEPROM(int& keySize) {
  keySize = (int)EEPROM.read(0);
  int* key = (int*)malloc(keySize * sizeof(int));

  for (int i = 0; i < keySize; i++) {
    key[i] = EEPROM.read(i + 1);
  }

  return key;
}
/* EEPROM UTILS -- END */

/* RFID UTILS -- START */
int writeToRFID(int* keyToWrite, int keySize) { // Modified MFRID Library Example Code
  byte* keyInBytes = intArrToByteArr(keyToWrite, keySize);

  // Look for new cards
  if ( ! mfrc522.PICC_IsNewCardPresent())
    return 0;

  // Select one of the cards
  if ( ! mfrc522.PICC_ReadCardSerial())
    return 0;

  MFRC522::PICC_Type piccType = mfrc522.PICC_GetType(mfrc522.uid.sak);

  // Check for compatibility
  if (	  piccType != MFRC522::PICC_TYPE_MIFARE_MINI
          &&  piccType != MFRC522::PICC_TYPE_MIFARE_1K
          &&  piccType != MFRC522::PICC_TYPE_MIFARE_4K) {
    Serial.println(F("This sample only works with MIFARE Classic cards."));
    return 0;
  }

  // In this sample we use the second sector,
  // that is: sector #1, covering block #4 up to and including block #7
  byte sector		  = 1;
  byte blockAddr	  = 4;
  byte dataBlock[]	  = { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
  memcpy(dataBlock, keyInBytes, keySize);

  byte trailerBlock   = 7;
  MFRC522::StatusCode status;
  byte buffer[18];
  byte size = sizeof(buffer);

  // Authenticate using key B
  Serial.println(F("Authenticating again using key B..."));
  status = (MFRC522::StatusCode) mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_B, trailerBlock, &key, &(mfrc522.uid));
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("PCD_Authenticate() failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));
    return 0;
  }

  // Write data to the block
  Serial.print(F("Writing data into block ")); Serial.print(blockAddr);
  Serial.println(F(" ..."));
  dump_byte_array(dataBlock, 16); Serial.println();
  status = (MFRC522::StatusCode) mfrc522.MIFARE_Write(blockAddr, dataBlock, 16);
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("MIFARE_Write() failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));
  }
  Serial.println();

  // Read data from the block (again, should now be what we have written)
  Serial.print(F("Reading data from block ")); Serial.print(blockAddr);
  Serial.println(F(" ..."));
  status = (MFRC522::StatusCode) mfrc522.MIFARE_Read(blockAddr, buffer, &size);
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("MIFARE_Read() failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));
  }
  Serial.print(F("Data in block ")); Serial.print(blockAddr); Serial.println(F(":"));
  dump_byte_array(buffer, 16); Serial.println();

  // Check that data in block is what we have written
  // by counting the number of bytes that are equal
  Serial.println(F("Checking result..."));
  byte count = 0;
  for (byte i = 0; i < 16; i++) {
    // Compare buffer (= what we've read) with dataBlock (= what we've written)
    if (buffer[i] == dataBlock[i])
      count++;
  }
  Serial.print(F("Number of bytes that match = ")); Serial.println(count);
  if (count == 16) {
    Serial.println(F("Success :-)"));
  } else {
    Serial.println(F("Failure, no match :-("));
    Serial.println(F("	perhaps the write didn't work properly..."));
  }
  Serial.println();

  // Dump the sector data
  Serial.println(F("Current data in sector:"));
  mfrc522.PICC_DumpMifareClassicSectorToSerial(&(mfrc522.uid), &key, sector);
  Serial.println();

  // Halt PICC
  mfrc522.PICC_HaltA();
  // Stop encryption on PCD
  mfrc522.PCD_StopCrypto1();
}

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

byte* readFromRFID(int& keySize) { // Modified MFRID Library Example Code
  // Look for new cards
  if ( ! mfrc522.PICC_IsNewCardPresent())
    return 0;

  // Select one of the cards
  if ( ! mfrc522.PICC_ReadCardSerial())
    return 0;

  MFRC522::PICC_Type piccType = mfrc522.PICC_GetType(mfrc522.uid.sak);

  // Check for compatibility
  if (      piccType != MFRC522::PICC_TYPE_MIFARE_MINI
            &&  piccType != MFRC522::PICC_TYPE_MIFARE_1K
            &&  piccType != MFRC522::PICC_TYPE_MIFARE_4K) {
    Serial.println(F("This sample only works with MIFARE Classic cards."));
    return 0;
  }

  // In this sample we use the second sector,
  // that is: sector #1, covering block #4 up to and including block #7
  byte sector         = 1;
  byte blockAddr      = 4;
  byte trailerBlock   = 7;
  MFRC522::StatusCode status;
  byte buffer[18];
  byte size = sizeof(buffer);

  // Authenticate using key A
  Serial.println(F("Authenticating using key A..."));
  status = (MFRC522::StatusCode) mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, trailerBlock, &key, &(mfrc522.uid));
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("PCD_Authenticate() failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));
    return 0;
  }

  // Read data from the block
  Serial.print(F("Reading data from block ")); Serial.print(blockAddr);
  Serial.println(F(" ..."));
  status = (MFRC522::StatusCode) mfrc522.MIFARE_Read(blockAddr, buffer, &size);
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("MIFARE_Read() failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));
  }
  Serial.print(F("Data in block ")); Serial.print(blockAddr); Serial.println(F(":"));
  dump_byte_array(buffer, size); Serial.println();
  Serial.println();

  // Give them some values fam
  byte* readKey = (byte*)malloc(sizeof(byte) * 4);
  memcpy(readKey, buffer, sizeof(byte) * 4);
  keySize = sizeof(byte) * 4;


  // Halt PICC
  mfrc522.PICC_HaltA();
  // Stop encryption on PCD
  mfrc522.PCD_StopCrypto1();

  return readKey;
}

/**
   Helper routine to dump a byte array as hex values to Serial.
*/
void dump_byte_array(byte *buffer, byte bufferSize) {
  for (byte i = 0; i < bufferSize; i++) {
    Serial.print(buffer[i] < 0x10 ? " 0" : " ");
    Serial.print(buffer[i], HEX);
  }
}
/* RFID UTILS -- END */
